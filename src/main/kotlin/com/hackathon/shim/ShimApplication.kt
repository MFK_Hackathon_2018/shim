package com.hackathon.shim

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.kafka.annotation.KafkaListener

@SpringBootApplication
class ShimApplication {

    @KafkaListener(topics = ["testTopic"])
    fun listen(consumerRecord: ConsumerRecord<Any, Any>) {
        println(consumerRecord.toString())
    }
}

fun main(args: Array<String>) {
    runApplication<ShimApplication>(*args)
}
